import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import HorizontalLabelPositionBelowStepper from "./Step.js";
import SimpleAlerts from "./Alert.js";
import Alert from '@material-ui/lab/Alert';
import MultilineTextFields from "./TextField.js";
import DatePickers from "./Date.js";
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import { Progress } from 'antd';
import 'antd/dist/antd.css';
import './index.css';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
	root: {
		width: '100%',
		flexGrow: 1,
		flexWrap: 'wrap',


	},


});

export default function ActionsInExpansionPanelSummary() {
	const classes = useStyles();

	return (
		<div>
			<ExpansionPanel>
				<ExpansionPanelSummary
					expandIcon={<ExpandMoreIcon />}
					aria-label="Expand"
					aria-controls="additional-actions1-content"
					id="additional-actions1-header"
				>
					<FormControlLabel
						aria-label="Acknowledge"
						onClick={(event) => event.stopPropagation()}
						onFocus={(event) => event.stopPropagation()}
						// control={<Checkbox />}
						// control={<Card>
						//     <Alert severity="info"></Alert>
						//     <DatePickers /> 
						//     tugas 1
						//         </Card>}
						control={
							<Grid container className={classes.root} spacing={1}>
								<Grid item xs={3} container justify="center" >
									<div>start date</div>
									<DatePickers />
								</Grid>
								<Grid item xs={3} container justify="center" >
									<div>end date</div>
									<DatePickers />
								</Grid>
								<Grid item xs={1} container justify="center" >
									<Progress type="circle" width={60} percent={75} format={percent => `${percent} Days`} />
								</Grid>
								<Grid item xs={1} container justify="center" >
									<Progress type="circle" width={60} percent={75} />
								</Grid  >
								<Grid item xs={4} container ><Paper>tugas 1aaaaaaaasssssssssssssssaaaaaaaaaaaa aaaaaaaaassssssssssssssss ssssssaaaa</Paper></Grid>
							</Grid>
						}
					/>
				</ExpansionPanelSummary>
				<ExpansionPanelDetails>
					<HorizontalLabelPositionBelowStepper />
					<Typography color="textSecondary">
					</Typography>

				</ExpansionPanelDetails>
				<MultilineTextFields />
			</ExpansionPanel>
			<ExpansionPanel>
				<ExpansionPanelSummary
					expandIcon={<ExpandMoreIcon />}
					aria-label="Expand"
					aria-controls="additional-actions2-content"
					id="additional-actions2-header"
				>
					<FormControlLabel
						aria-label="Acknowledge"
						onClick={(event) => event.stopPropagation()}
						onFocus={(event) => event.stopPropagation()}
						control={

							<Grid container className={classes.root} spacing={1}>
								<Grid item xs={3} container justify="center" >

									<div>start date</div>
									<DatePickers />

								</Grid>

								<Grid item xs={3} container justify="center" >

									<div>end date</div>
									<DatePickers />

								</Grid>

								<Grid item xs={1} container justify="center" >

									<Progress type="circle" width={60} percent={75} format={percent => `${percent} Days`} />


								</Grid>
								<Grid item xs={1} container justify="center" >

									<Progress type="circle" width={60} percent={100} status="success" />


								</Grid  >

								<Grid item xs={4} container ><Paper>tugas 2 aaaaaaaasssssssssssssssaaaaaaaaaaaa aaaaaaaaassssssssssssssss ssssssaaaa</Paper></Grid>

							</Grid>
						}


					/>
				</ExpansionPanelSummary>
				<ExpansionPanelDetails>

					{/* <Typography color="textSecondary">

                        The focus event of the nested action will propagate up and also focus the expansion
                        panel unless you explicitly stop it.
          </Typography> */}
				</ExpansionPanelDetails>
				<HorizontalLabelPositionBelowStepper />
				<MultilineTextFields />
			</ExpansionPanel>
			<ExpansionPanel>
				<ExpansionPanelSummary
					expandIcon={<ExpandMoreIcon />}
					aria-label="Expand"
					aria-controls="additional-actions3-content"
					id="additional-actions3-header"
				>
					<FormControlLabel
						aria-label="Acknowledge"
						onClick={(event) => event.stopPropagation()}
						onFocus={(event) => event.stopPropagation()}
						control={

							<Grid container className={classes.root} spacing={1}>
								<Grid item xs={3} container justify="center" >

									<div>start date</div>
									<DatePickers />

								</Grid>

								<Grid item xs={3} container justify="center" >

									<div>end date</div>
									<DatePickers />

								</Grid>

								<Grid item xs={1} container justify="center" >

									<Progress type="circle" width={60} percent={75} format={percent => `${percent} Days`} />


								</Grid>
								<Grid item xs={1} container justify="center" >

									<Progress type="circle" width={60} percent={75} status="exception" />


								</Grid  >

								<Grid item xs={4} container ><Paper>tugas 3 aaaaaaaasssssssssssssssaaaaaaaaaaaa aaaaaaaaassssssssssssssss ssssssaaaa</Paper></Grid>

							</Grid>
						}


					/>
				</ExpansionPanelSummary>
				<ExpansionPanelDetails>

					<Typography color="textSecondary">
						If you forget to put an aria-label on the nested action, the label of the action will
						also be included in the label of the parent button that controls the panel expansion.
                    </Typography>
				</ExpansionPanelDetails>
				<HorizontalLabelPositionBelowStepper />

				<MultilineTextFields />
			</ExpansionPanel>
		</div>
	);
}