import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Card from "../../components/Card/Card.js";
import CardHeader from "../../components/Card/CardHeader.js";
import CardBody from "../../components/Card/CardBody.js";
import HorizontalLabelPositionBelowStepper from "./Component/Step.js";
import VerticalTabs from "./Component/VerticalTab.js";
import CheckboxLabels from "./Component/Check.js";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"

  },
}));

export default function FullWidthGrid() {
  const classes = useStyles();

  return (
    <div className={classes.root}>

      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Card>
            <CardHeader  color="primary">
              <h4 className={classes.cardTitleWhite} >Material Dashboard Heading rw</h4>
              <p className={classes.cardCategoryWhite}>
                Created using Roboto Font Family
        </p>
            </CardHeader>
            <VerticalTabs />

          </Card>
        </Grid>
        <Grid item xs={6}>
          <Card>
            {/* <CheckboxLabels /> */}
            tes
          </Card>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Card>

          </Card>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>xs=12 sm=6</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>xs=6 sm=3</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>xs=6 sm=3</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>xs=6 sm=3</Paper>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Paper className={classes.paper}>xs=6 sm=3</Paper>
        </Grid>
      </Grid>

    </div>



  );
}