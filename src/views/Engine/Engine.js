import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Upload, message, Button } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import Card from '@material-ui/core/Card';
import DatePickers from "./Component/Date.js";
import { Progress } from 'antd';
import 'antd/dist/antd.css';
import './Component/index.css';
import TextField from '@material-ui/core/TextField';
import { lightBlue } from '@material-ui/core/colors';
import CustomizedDialogs from "./Component/Dialog.js";
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: lightBlue[500],
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },

}))(TableCell);


const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

function createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
}

const rows = [
    createData('proyek A', 159, 6.0, 24, 4.0),
    createData('Proyek B', 237, 9.0, 37, 4.3),
    createData('Proyek C', 262, 16.0, 24, 6.0),
    createData('Proyek D', 305, 3.7, 67, 4.3),
    createData('Proyek E', 356, 16.0, 49, 3.9),
];

const useStyles = makeStyles({
    table: {
        minWidth: 700,
    },
    width: '75ch',

});

export default function CustomizedTables() {
    const classes = useStyles();
    const props = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };
    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
                <TableHead>
                    <TableRow>

                        <StyledTableCell align="center"> proyek</StyledTableCell>
                        <StyledTableCell align="center">start date</StyledTableCell>
                        <StyledTableCell align="center">end date</StyledTableCell>
                        <StyledTableCell align="center">days left</StyledTableCell>
                        <StyledTableCell align="center">status</StyledTableCell>
                        <StyledTableCell align="center">keterangan</StyledTableCell>
                        <StyledTableCell align="center">unggah dokumen</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <StyledTableRow key={row.name} align="center">
                            <StyledTableCell component="th" scope="row" align="center">{row.name}<div>sub proyek</div></StyledTableCell>
                            <StyledTableCell component="th" scope="row" align="center">
                                <Typography  align="center" style={{ backgroundColor: '#cfe8fc', height: '5vh' }}>11/10/2020</Typography>
                            </StyledTableCell>
                            <StyledTableCell component="th" scope="row" align="center"> 
                                <Typography  align="center" style={{ backgroundColor: '#cfe8fc', height: '5vh' }} >11/11/2020</Typography>
                            </StyledTableCell>
                            <StyledTableCell align="center"><Progress type="circle" width={60} percent={75} format={percent => `${percent} Days`} /></StyledTableCell>
                            <StyledTableCell align="center"><Progress type="circle" width={60} percent={100}  /></StyledTableCell>
                            <StyledTableCell align="center">
                                <CustomizedDialogs />
                            </StyledTableCell>
                            <StyledTableCell align="center"><Upload {...props}><Button><UploadOutlined /> Click to Upload </Button></Upload>   </StyledTableCell>
                        </StyledTableRow>
                    ))}

                </TableBody>
            </Table>
        </TableContainer>

    );
}
